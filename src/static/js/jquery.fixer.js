/*!
 * jquery.fixer.js 0.0.3 - https://github.com/yckart/jquery.fixer.js
 * Fix elements as `position:sticky` do.
 *
 *
 * Copyright (c) 2013 Yannick Albert (http://yckart.com/) | @yckart
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php).
 * 2013/07/02
 **/

;(function($, window) {

    var $win = $(window);
    var defaults = {
        // classElementWidth: 'advert-item',
        // forceFixed: false
        // gap: 0,
        horizontal: false,
        isFixed: $.noop
    };

    var supportSticky = function(elem) {
        var prefixes = ['', '-webkit-', '-moz-', '-ms-', '-o-'], prefix;
        while (prefix = prefixes.pop()) {
            elem.style.cssText = 'position:' + prefix + 'sticky';
            if (elem.style.position !== '') return true;
        }
        return false;
    };

    $.fn.fixer = function(options) {
        options = $.extend(options, defaults);
        if (options.classElementWidth) {
            var width = $('.'+options.classElementWidth).width()+'px';
        } else if (options.ElementWidth) {
             var width = options.Width;
        } else {
             var width = '100%';
        }
        var hori = options.horizontal,
            cssPos = hori ? 'left' : 'top';
        var forceFixed = options.forceFixed;

        return this.each(function() {
            var style = this.style,
                $this = $(this),
                $parent = $this.parent();

            if (supportSticky(this)) {
                style[cssPos] = options.gap + 'px';
                return;
            }

            $win.on('scroll', function() {
                var scrollPos = $win[hori ? 'scrollLeft' : 'scrollTop'](),
                    elemSize = $this[hori ? 'outerWidth' : 'outerHeight'](),
                    parentPos = $parent.offset()[cssPos],
                    parentSize = $parent[hori ? 'outerWidth' : 'outerHeight']();
                while (parentSize == 1) {
                    $parent = $parent.parent();
                    parentSize = $parent[hori ? 'outerWidth' : 'outerHeight']();
                }

                style.width = width;
                style.position = 'absolute';

                if (scrollPos >= parentPos - options.gap && ((parentSize + parentPos - options.gap) >= (scrollPos + elemSize)|| forceFixed)) {
                    style.position = 'fixed';
                    style[cssPos] = options.gap + 'px';
                    options.isFixed();
                } else if (scrollPos < parentPos) {
                    style[cssPos] = 0;
                } else {
                    style[cssPos] = parentSize - elemSize + 'px';
                }
            }).resize();
        });
    };

}(jQuery, this));
