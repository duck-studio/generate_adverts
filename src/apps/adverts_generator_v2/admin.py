from django.contrib import admin
from django.db import models
from django import forms

from . models import *


class GroupInPhraseGroupSetInline(admin.TabularInline):
    model = GroupInPhraseGroupSet
    extra = 0
    min_num = 1


@admin.register(PhraseGroupSet)
class PhraseGroupSetAdmin(admin.ModelAdmin):
    list_display = ('title', )
    inlines = (GroupInPhraseGroupSetInline, )


class PhraseInline(admin.TabularInline):
    model = Phrase
    extra = 0
    min_num = 1

    formfield_overrides = {
        models.TextField: {'widget': forms.TextInput(attrs={'style': 'width:99%'})},
    }


@admin.register(PhraseGroup)
class PhraseGroupAdmin(admin.ModelAdmin):
    list_display = ('title', )
    inlines = (PhraseInline, )


@admin.register(GroupInPhraseGroupSet)
class GroupInPhraseGroupSetAdmin(admin.ModelAdmin):
    list_display = ('phrase_groupset', 'phrase_group', 'order')


@admin.register(Phrase)
class PhraseAdmin(admin.ModelAdmin):
    list_display = ('group', 'text', )
    list_filter = ('group', )
