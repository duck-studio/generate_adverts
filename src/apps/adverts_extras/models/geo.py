from django.db import models

__all__ = ('City', 'District', 'Street', 'Metro')


class City(models.Model):
    title = models.CharField(verbose_name='Название', max_length=100)

    class Meta:
        verbose_name = 'город'
        verbose_name_plural = 'города'

    def __str__(self):
        return self.title


class District(models.Model):
    city = models.ForeignKey(City, verbose_name='Город')
    title = models.CharField(verbose_name='Название', max_length=100)

    class Meta:
        verbose_name = 'район'
        verbose_name_plural = 'районы'

    def __str__(self):
        return self.title


class Street(models.Model):
    district = models.ForeignKey(District, verbose_name='Район')
    title = models.CharField(verbose_name='Название', max_length=100)

    def city(self):
        return self.district.city
    city.short_description = 'Город'

    class Meta:
        verbose_name = 'улица'
        verbose_name_plural = 'улицы'

    def __str__(self):
        return self.title


class House(models.Model):
    street = models.ForeignKey(Street, verbose_name='Улица')
    title = models.CharField(verbose_name='Номер дома', max_length=100)
    floors_total = models.PositiveSmallIntegerField(verbose_name='Этажность', default=0)

    def street_and_house(self):
        return self.street.title + ' ' + self.title
    street_and_house.short_description = 'Улица и номер дома'

    class Meta:
        verbose_name = 'номер дома'
        verbose_name_plural = 'номера домов'

    def __str__(self):
        return self.title


class Metro(models.Model):
    city = models.ForeignKey(City, verbose_name='Город')
    title = models.CharField(verbose_name='Название', max_length=100)

    class Meta:
        verbose_name = 'станция метро'
        verbose_name_plural = 'станции метро'

    def __str__(self):
        return self.title
