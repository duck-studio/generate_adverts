from django.apps import apps
from django.contrib import admin
from django.db import models
from django import forms

from apps.adverts_extras.models.geo import City, District, Street, House, Metro


# for model in apps.get_app_config('adverts_extras').get_models():
#     admin.site.register(model)


class HouseInline(admin.TabularInline):
    model = House
    extra = 0
    min_num = 1

    formfield_overrides = {
        models.TextField: {'widget': forms.TextInput(attrs={'style': 'width:99%'})},
    }
@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = ('title', )

@admin.register(District)
class DistrictAdmin(admin.ModelAdmin):
    list_display = ('title', 'city', )

@admin.register(Street)
class StreetAdmin(admin.ModelAdmin):
    list_display = ('title', 'district', 'city')
    inlines = (HouseInline, )

@admin.register(House)
class HouseAdmin(admin.ModelAdmin):
    list_display = ('street_and_house', 'title', 'floors_total' )

@admin.register(Metro)
class MetroAdmin(admin.ModelAdmin):
    list_display = ('title', 'city' )
