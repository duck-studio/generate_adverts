from django.apps import AppConfig


class AdvertsExtrasConfig(AppConfig):
    name = 'apps.adverts_extras'
    verbose_name = 'Дополнительные данные'

