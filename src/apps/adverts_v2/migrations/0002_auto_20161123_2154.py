# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-11-23 21:54
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('adverts_extras', '0004_auto_20160921_1334'),
        ('adverts_v2', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='CategoryParsingSettings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('price_from', models.PositiveIntegerField(default=0, verbose_name='Цена от')),
                ('price_to', models.PositiveIntegerField(default=0, verbose_name='Цена до')),
                ('price_step', models.PositiveIntegerField(default=1, validators=[django.core.validators.MinValueValidator(1)], verbose_name='Шаг цены')),
                ('parser_name', models.CharField(max_length=100, verbose_name='Имя парсера')),
                ('parser_url', models.TextField(verbose_name='Ссылка')),
                ('parser_pages_limit', models.PositiveSmallIntegerField(default=5, verbose_name='Количество страниц')),
                ('is_active', models.BooleanField(default=True, verbose_name='Активность')),
            ],
            options={
                'verbose_name_plural': 'настройки парсинга для категорий',
                'verbose_name': 'настройка парсинга для категории',
            },
        ),
        migrations.CreateModel(
            name='CottageAdvert',
            fields=[
                ('advert_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='adverts_v2.Advert')),
                ('area_house', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Площадь дома')),
                ('area_land', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Площадь участка')),
                ('wall_material', models.PositiveSmallIntegerField(blank=True, choices=[(1, 'Кирпич'), (2, 'Блоки'), (3, 'Панели'), (4, 'Монолит')], null=True, verbose_name='Материал стен')),
                ('number_floors', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Кол-во этажей')),
            ],
            options={
                'verbose_name_plural': 'объявления о продаже коттеджей',
                'verbose_name': 'объявление о продаже коттеджа',
            },
            bases=('adverts_v2.advert',),
        ),
        migrations.RemoveField(
            model_name='category',
            name='form_class_name',
        ),
        migrations.AddField(
            model_name='advert',
            name='title_original',
            field=models.TextField(blank=True, verbose_name='оригинальный заголовок'),
        ),
        migrations.AddField(
            model_name='advertphoto',
            name='is_main',
            field=models.BooleanField(default=False, verbose_name='Главная'),
        ),
        migrations.AlterField(
            model_name='advert',
            name='donor',
            field=models.PositiveSmallIntegerField(choices=[(0, 'est.ua'), (1, 'printerest.com'), (2, 'tiu.ru'), (3, 'avito.ru'), (999, 'DUMMY')], verbose_name='Донор'),
        ),
        migrations.AddField(
            model_name='categoryparsingsettings',
            name='category',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='adverts_v2.Category', verbose_name='Категория'),
        ),
        migrations.AddField(
            model_name='categoryparsingsettings',
            name='city',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='adverts_extras.City', verbose_name='Город'),
        ),
    ]
