from django.shortcuts import redirect


class GlobalLoginRequiredMiddleware:
    def process_view(self, request, *args, **kwargs):
        if request.resolver_match.url_name == 'login' or request.user.is_authenticated():
            return None
        return redirect('main:login')
