from .settings import *

DEBUG = False

SECRET_KEY = 'kul%w9j2&o*4ke_=#_#9z8-yse93u7q8y&+p5p85e7wrhav*pr'

ALLOWED_HOSTS = ['5.187.0.6', ]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'generate_adverts',
        'USER' : 'generate_adverts',
        'PASSWORD' : 'password',
        'HOST' : 'localhost',
        'PORT' : '5432',
    }
}
