from scrapy import Request

from apps.adverts_v2.options import TIU
from ...helpers import normalize_string, normalize_int
from ...base_spider import BaseSpider

__all__ = ('TiuSpider', )


PAGES_LIMIT = 5

COLLECT_LINKS_SELECTOR = '//a[@class="b-image-holder js-favourites-popup"]/@href'
COLLECT_IMAGES_SELECTOR = '//div[@class="b-product-images"]//a/@href'
NEXT_PAGE_SELECTOR = '//a[@class=" b-pager__link b-pager__link_type_nav pager_lastitem"]/@href'
DESCRIPTION_SELECTOR = '//div[@class="b-user-content"]/p/text()'
TITLE_SELECTOR = '//h1[@class="b-product__name"]/span/text()'
PRICE_SELECTOR = '//span[@itemprop="price"]/text()'


class TiuSpider(BaseSpider):
    name = 'tiu-spider'
    allowed_domains = ('tiu.ru', 'images.ru.prom.st', )
    donor = TIU

    custom_settings = {
        'ITEM_PIPELINES': {
            'parsing.common_pipelines.MakeInitialChecks': 10,
            'parsing.common_pipelines.BuildCommonData': 20,
            'parsing.common_pipelines.ValidateAndSaveItem': 100,
            'parsing.common_pipelines.ProcessItemPhotos': 150,
        },
        'CONCURRENT_REQUESTS': 5,
        'IMAGES_CROP': {
            'bottom': '20%',
        },
    }

    def parse_list(self, response):
        adverts_links = response.xpath(COLLECT_LINKS_SELECTOR).extract()
        if not adverts_links:
            self.logger.warning('No adverts links founded on page {}'.format(response.url))

        for link in adverts_links:
            yield Request(link + '?no_redirect=1', callback=self.parse_item)

        next_page_link = response.xpath(NEXT_PAGE_SELECTOR).extract_first()
        if next_page_link:
            yield Request('http://tiu.ru' + next_page_link, callback=self.parse, dont_filter=True)

    def parse_item(self, response):
        item = super().parse_item(response)
        item['price'] = normalize_int(response.xpath(PRICE_SELECTOR).extract_first())
        item['title_original'] = normalize_string(response.xpath(TITLE_SELECTOR).extract_first())
        item['description_original'] = ' '.join(response.xpath(DESCRIPTION_SELECTOR).extract()).strip()
        item['image_urls'] = response.xpath(COLLECT_IMAGES_SELECTOR).extract()
        return item
