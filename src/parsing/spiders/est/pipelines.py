import random
from apps.adverts_v2.options import HOUSE_MATERIALS_VALUES
from apps.adverts_extras.models import Street

HOUSE_NUMBERS_RANGE = range(1, 101) # от 1 до 100


class PrepareItemData(object):
    def process_item(self, item, spider):

        # округляем цены до ближайшего числа, кратного 10 (точный алгоритм пока не известен)
        price = item['price']
        if price:
            item['price'] = round(price, -1)

        # Материал дома
        item['house_material'] = random.choice(HOUSE_MATERIALS_VALUES)

        # Догенериваем адрес
        if item.get('district'):
            item['street'] = Street.objects.filter(district=item['district']).order_by('?').first()

        if item.get('street'):
            item['house_number'] = random.choice(HOUSE_NUMBERS_RANGE)

        return item
