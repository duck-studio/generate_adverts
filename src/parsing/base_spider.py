import os

from scrapy import Spider
from scrapy.exceptions import NotConfigured
from scrapy.utils.project import get_project_settings

from apps.adverts_extras.models import City
from apps.adverts_v2.models import Category
from .items import AdvertItem


class SpiderSettingsMetaclass(type):
    def __new__(cls, name, bases, attrs):
        scrapy_data_dir = get_project_settings()['SCRAPY_DATA_DIR']
        settings_spider = attrs.get('custom_settings', {}).copy()

        if 'IMAGES_STORE' not in settings_spider:
            settings_spider['IMAGES_STORE'] = os.path.join(scrapy_data_dir, attrs['name'], 'downloads')

        if 'JOBDIR' not in settings_spider:
            settings_spider['JOBDIR'] = os.path.join(scrapy_data_dir, attrs['name'], 'jobdir')

        attrs['custom_settings'] = settings_spider
        return super().__new__(cls, name, bases, attrs)


class BaseSpider(Spider, metaclass=SpiderSettingsMetaclass):
    item_class = AdvertItem
    name = 'base-parser'
    donor = None

    def __init__(self, category, url, name=None, city=None, pages_limit=5, extra_context=None, **kwargs):
        super().__init__(name=name, **kwargs)

        try:
            self.category = Category.objects.get(alias=category)
        except Category.DoesNotExist:
            raise NotConfigured('Не найдена категория с алиасом {}'.format(category))

        self.start_urls = [url]
        self.city = city
        self.pages_limit = int(pages_limit)
        self.current_page = 2
        self.extra_context = extra_context or {}

    def parse(self, response):
        if not self.pages_limit or self.pages_limit >= self.current_page:
            self.current_page += 1
            return self.parse_list(response)

    def parse_list(self, response):
        pass

    def parse_item(self, response):
        return self.item_class(donor=self.donor, donor_url=response.url, category=self.category, city=self.city)
