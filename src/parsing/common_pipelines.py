import os
import random
from io import BytesIO

from PIL import Image, ImageOps
from django.core.exceptions import ValidationError
from django.core.files.base import File
from django.utils import six
from scrapy.exceptions import DropItem
from scrapy.pipelines.images import ImagesPipeline, ImageException

from apps.adverts_extras.models import Metro, District
from apps.adverts_v2.models import AdvertPhoto
from libs.utils import clearable_file


class MakeInitialChecks(object):
    def process_item(self, item, spider):
        # на всякий случай. поидее это лишнее
        if item.django_model.objects.filter(donor_url=item['donor_url']).exists():
            raise DropItem('Advert already exists in database')

        return item


class BuildCommonData(object):
    def process_item(self, item, spider):

        if spider.city:
            item['city'] = spider.city
            item['district'] = District.objects.filter(city=item['city']).order_by('?').first()
            item['metro'] = Metro.objects.filter(city=item['city']).order_by('?').first()

        if not item['price']:
            price_range = spider.extra_context.get('PRICE_RANGE')
            if price_range:
                item['price'] = random.choice(price_range)

        return item


class ValidateAndSaveItem(object):
    def process_item(self, item, spider):
        if not item.is_valid():
            raise DropItem('Validation errors: {}'.format(item.errors))

        item.save()
        return item


class ProcessItemPhotos(ImagesPipeline):
    def open_spider(self, spider):
        self.store.basedir = os.path.join(self.store.basedir, spider.category.alias.lower())
        super().open_spider(spider)

    def get_images(self, response, request, info):
        path = self.file_path(request, response=response, info=info)
        orig_image = Image.open(BytesIO(response.body))
        orig_image = ImageOps.mirror(orig_image)

        orig_image.rotate(angle=33, expand=True)

        width, height = orig_image.size
        if width < self.min_width or height < self.min_height:
            raise ImageException("Image too small (%dx%d < %dx%d)" % (width, height, self.min_width, self.min_height))

        crop_settings = info.spider.settings.get('IMAGES_CROP')
        if crop_settings is not None:
            left_crop = crop_settings.get('left', 0)
            try:
                left_crop = int(left_crop)
            except (ValueError, TypeError):
                left_crop = left_crop.replace('%', '')
                left_crop = width * int(left_crop) // 100

            top_crop = crop_settings.get('top', 0)
            try:
                top_crop = int(top_crop)
            except (ValueError, TypeError):
                top_crop = top_crop.replace('%', '')
                top_crop = height * int(top_crop) // 100

            right_crop = crop_settings.get('right', 0)
            try:
                right_crop = width - int(right_crop)
            except (ValueError, TypeError):
                right_crop = right_crop.replace('%', '')
                right_crop = width - width * int(right_crop) // 100

            bottom_crop = crop_settings.get('bottom', 0)
            try:
                bottom_crop = height - int(bottom_crop)
            except (ValueError, TypeError):
                bottom_crop = bottom_crop.replace('%', '')
                bottom_crop = height - height * int(bottom_crop) // 100

            orig_image = orig_image.crop((left_crop, top_crop, right_crop, bottom_crop))

        rotate_settings = info.spider.settings.get('IMAGES_ROTATE')
        if rotate_settings is not None:
            orig_image = orig_image.rotate(
                angle=rotate_settings.get('angle', 0),
                resample=rotate_settings.get('resample', Image.NEAREST),
                expand=rotate_settings.get('expand', 0)
            )

        enhance_settings = info.spider.settings.get('IMAGES_ENHANCE')
        if enhance_settings is not None:
            enhance_class = enhance_settings['enhancer']
            enhance_factor = enhance_settings['factor']
            orig_image = enhance_class(orig_image).enhance(enhance_factor)

        image, buf = self.convert_image(orig_image)
        yield path, image, buf

        for thumb_id, size in six.iteritems(self.thumbs):
            thumb_path = self.thumb_path(request, thumb_id, response=response, info=info)
            thumb_image, thumb_buf = self.convert_image(image, size)
            yield thumb_path, thumb_image, thumb_buf

    def item_completed(self, results, item, info):
        item = super().item_completed(results, item, info)

        for downloaded, download_info in results:
            if not downloaded:
                continue

            downloaded_image_path = download_info.get('path')
            if not downloaded_image_path:
                continue

            downloaded_image_checksum = download_info.get('checksum')
            if not downloaded_image_checksum or AdvertPhoto.objects.filter(checksum=downloaded_image_checksum).exists():
                continue

            downloaded_image_full_path = self.store._get_filesystem_path(downloaded_image_path)
            if not os.path.isfile(downloaded_image_full_path):
                continue

            with clearable_file(downloaded_image_full_path):
                new_photo = AdvertPhoto()
                new_photo.advert = item.instance
                new_photo.checksum = downloaded_image_checksum

                with open(downloaded_image_full_path, 'rb') as image_file:
                    try:
                        new_photo.image.save(os.path.basename(image_file.name), File(image_file), save=False)
                    except IOError:
                        continue

                    try:
                        new_photo.full_clean()
                    except ValidationError:
                        continue
                    else:
                        new_photo.save()

        if info.spider.settings.get('IMAGES_MIN_COUNT', 0) > item.instance.photos.count():
            item.instance.delete()
            raise DropItem('Too few pictures found for advert')

        return item