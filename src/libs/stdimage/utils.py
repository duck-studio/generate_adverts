import os
import hashlib
import operator
import itertools
from PIL import Image, ImageOps, ImageEnhance
from django.contrib.staticfiles import finders


# Действия при несовпадении размера
ACTION_CROP = 1
ACTION_CROP_ANYWAY = 2
ACTION_STRETCH_BY_WIDTH = 3
ACTION_INSCRIBE = 4
ACTION_INSCRIBE_RESIZE = 5
ACTIONS = (ACTION_CROP, ACTION_STRETCH_BY_WIDTH, ACTION_CROP_ANYWAY, ACTION_INSCRIBE, ACTION_INSCRIBE_RESIZE)

SOURCE_QUALITY = 100
DEFAULT_QUALITY = 90

JPEG_FORMAT = 'JPEG'
JPEG_SAVE_FORMAT = 'JPG'


DEFAULT_VARIATION = dict(
    # Размер (может быть функцией, принимающей instance модели)
    size=None,

    # Тактика при несовпадении размера:
    #   ACTION_CROP:
    #       а) Картинка больше - уменьшить до нужного размера по одной из сторон и обрезать излишки
    #       б) Картинка меньше - сохранить оригинал
    #   ACTION_STRETCH_BY_WIDTH:
    #       Растянуть картинку по ширине, оставив высоту пропорциональной.
    #       Высота в параметре size игнорируется.
    #   ACTION_CROP_ANYWAY:
    #       а) Картинка больше - уменьшить до нужного размера по одной из сторон и обрезать излишки
    #       б) Картинка меньше - растянуть и обрезать
    #   ACTION_INSCRIBE:
    #       а) Картинка больше - уменьшить до нужного размера по одной из сторон
    #       б) Картинка меньше - сохранить оригинал
    #       Создается фон нужного размера, в центр которого вписывается картинка
    #   ACTION_INSCRIBE_RESIZE:
    #       Картинка с сохранением пропорций и без обрезок вписывается в size,
    #       Итоговые размеры вариации могут отличаться от заданных в size, но не превышают их
    action=ACTION_CROP,

    # Цвет фона, на который накладывается изображение
    background=(255, 255, 255, 0),

    # Файл-маска для обрезания картинки
    mask='',

    # Файл, накладываемый на картинку
    overlay='',

    # Настройки наложения водяного знака.
    # Например:
    #   watermark = {
    #       file: 'img/watermark.png',
    #       position: 'BR',
    #       padding: (20, 30),
    #       opacity: 1,
    #       scale: 1,
    #   }
    watermark=None,
    
    # Требуемый формат изображения
    format='',

    # Использовать для нарезки исходную картинку (игнорирование кропа админки)
    use_source=False,

    # Качество результата картинки
    quality=DEFAULT_QUALITY,

    # Ссылка на другую вариацию. Новое изображение не создается
    alias_for='',
)


DEFAULT_WATERMARK = {
    'file': '',
    'position': 'BR',
    'padding': (0, 0),
    'opacity': 1,
    'scale': 1,
}


def get_variation_by_name(variations, name):
    """ Получение вариации по имени """
    variation = next((item for item in variations if item['name'] == name), None)
    if variation:
        alias_for = variation.get('alias_for')
        if alias_for:
            return get_variation_by_name(variations, alias_for)
    return variation


def format_variations(variations):
    """ Проверка и форматирование вариаций """
    variations = variations or {}
    
    result = []
    for name, params in variations.items():
        if isinstance(params, (list, tuple)):
            params = {
                'size': params,
            }

        if not params or not isinstance(params, dict):
            continue

        final_params = dict(DEFAULT_VARIATION,
                            name=name,
                            **params)

        if final_params['alias_for']:
            result.append(final_params)
            continue
        
        if not final_params.get('size'):
            raise ValueError('Не указан размер для вариации %r' % name)
        elif not isinstance(final_params['size'], (list, tuple)) and not callable(final_params['size']):
            raise ValueError('Некоррекно задан размер вариации')
            
        if final_params['action'] not in ACTIONS:
            raise ValueError('Задан некорректный алгоритм обрезки картинок')

        # Проверка формата
        image_format = final_params.get('format', '').upper()
        if image_format:
            if image_format in ('JPG', 'JPEG'):
                image_format = 'JPEG'

            if image_format not in ('PNG', 'JPEG', 'GIF'):
                raise TypeError('Формат изображений %r не поддерживается' % image_format)
        elif final_params.get('mask'):
            # Не указан формат, но указана маска - переводим в PNG
            image_format = 'PNG'
        final_params['format'] = image_format

        # Overlay
        overlay = final_params.get('overlay', '')
        if overlay:
            overlay_path = finders.find(overlay)
            if not overlay_path:
                raise ValueError('Не найден файл оверлея: %s' % overlay)
            else:
                final_params['overlay'] = overlay_path
        
        # Mask
        mask = final_params.get('mask', '')
        if mask:
            mask_path = finders.find(mask)
            if not mask_path:
                raise ValueError('Не найден файл маски: %s' % mask)
            else:
                final_params['mask'] = mask_path
        
        # Водяной знак
        watermark = final_params.pop('watermark', {})
        if watermark:
            watermark = dict(DEFAULT_WATERMARK, **watermark)
            
            if 'file' not in watermark:
                raise ValueError('Не указан файл водяного знака')
            watermark_path = finders.find(watermark['file'])
            if not watermark_path:
                raise ValueError('Не найден файл водяного знака: %s' % watermark['file'])
            else:
                watermark['file'] = watermark_path
            
            if not isinstance(watermark['padding'], (list, tuple)):
                raise ValueError('Некорректное значение отступов водяного знака')
            try:
                watermark['padding'] = tuple(map(int, watermark['padding']))
            except (ValueError, TypeError):
                raise ValueError('Некорректное значение отступов водяного знака')
            
            watermark['opacity'] = float(watermark['opacity'])
            if watermark['opacity'] < 0 or watermark['opacity'] > 1:
                raise ValueError('Некорректное значение непрозрачности водяного знака')
                
            watermark['scale'] = float(watermark['scale'])
            if watermark['scale'] < 0:
                raise ValueError('Некорректное значение масштабирования водяного знака')
        
            watermark['position'] = watermark['position'].upper()
            if watermark['position'] not in ('TL', 'TR', 'BL', 'BR', 'C'):
                raise ValueError('Некорректное значение позиции водяного знака')
            
            final_params['watermark'] = watermark
        
        result.append(final_params)
    
    return result


def upload_chunked_file(request, directory, file):
    chunk_count = int(request.POST.get('chunks', 1))
    chunk_num = int(request.POST.get('chunk', 0))

    file_name = request.POST.get('name', file.name)
    file_hash = '%s.%s' % (request.session.session_key, file_name)
    file_hash = hashlib.md5(file_hash.encode()).hexdigest()
    filename, ext = os.path.splitext(file_name)
    filepath = os.path.join(directory, file_hash + ext)

    # Записываем чанки на диск
    with open(filepath, 'wb+' if chunk_num == 0 else 'ab+') as dest:
        for chunk in file.chunks():
            dest.write(chunk)

    return filepath, chunk_num == chunk_count-1

    
def put_on_bg(image, bg_size, bg_color, masked=False):
    """ Создание фона цветом bg_color и размера bg_size, на который будет наложена картинка image """
    background = Image.new('RGBA', bg_size, bg_color)
    size_diff = list(itertools.starmap(operator.sub, zip(background.size, image.size)))
    offset = (size_diff[0]//2, size_diff[1]//2)
    if masked:
        background.paste(image, offset, image)
    else:
        background.paste(image, offset)
    return background
    

def limited_size(size, limit_size):
    """ 
    Пропорциональное уменьшение размера size, чтобы он не превосходил limit_size.
    Допустимо частичное указание limit_size, например, (1024, 0).
    Если size умещается в limit_size, возвращает None
    """
    max_width, max_height = limit_size
    if not max_width and not max_height:
        return None
        
    width, height = size
    if max_width and width > max_width:
        height = height * (max_width / width)
        width = max_width
    if max_height and height > max_height:
        width = width * (max_height / height)
        height = max_height
    width, height = round(width), round(height)
    if size == (width, height):
        return None
    
    return width, height 
    

def variation_crop(image, crop=None):
    """ Обрезка по координатам """
    if not crop:
        return image
    
    left, top, width, height = crop
    if left < 0:
        left = 0
    if left + width > image.size[0]:
        width = image.size[0] - left

    if top < 0:
        top = 0
    if top + height > image.size[1]:
        height = image.size[1] - top

    return image.crop((
        left,
        top,
        left + width,
        top + height))


def variation_resize(instance, image, variation, target_format):
    """
        Изменение размера в соответствии с variation[action] и variation[size]
    """
    target_bgcolor = variation.get('background')
    
    # Целевой размер
    target_size = variation.get('size', (0, 0))
    if callable(target_size):
        target_size = target_size(instance)
    if not isinstance(target_size, (list, tuple)) or len(target_size) != 2:
        raise ValueError('Размер картинки некорректен: %r' % target_size)
    
    # Отношение площадей исходника и вариации
    relation = min(itertools.starmap(operator.truediv, zip(image.size, target_size)))
    relation /= 4
    if relation > 1:
        middle_size = tuple(int(p/relation) for p in image.size)
        image = image.resize(middle_size, Image.NEAREST)
    
    # Действие при несовпадении размера
    target_action = variation.get('action', ACTION_CROP)
    if target_action == ACTION_CROP:
        if image.size[0] < target_size[0] and image.size[1] < target_size[1]:
            # Если картинка меньше - оставляем её как есть
            pass
        elif image.size[0] < target_size[0] or image.size[1] < target_size[1]:
            # Если она меньше по одной стороне - отрезаем излишки у другой
            image.thumbnail(target_size, resample=Image.ANTIALIAS)
        else:
            image = ImageOps.fit(image, target_size, method=Image.ANTIALIAS)
            
        # При сохранении PNG/GIF в JPEG прозрачный фон становится черным. Накладываем на фон
        if image.mode == 'RGBA' and target_format == 'JPEG':
            image = put_on_bg(image, image.size, target_bgcolor, masked=True)
    elif target_action == ACTION_CROP_ANYWAY:
        image = ImageOps.fit(image, target_size, method=Image.ANTIALIAS)

        # При сохранении PNG/GIF в JPEG прозрачный фон становится черным. Накладываем на фон
        if image.mode == 'RGBA' and target_format == 'JPEG':
            image = put_on_bg(image, image.size, target_bgcolor, masked=True)
    elif target_action == ACTION_STRETCH_BY_WIDTH:
        img_aspect = operator.truediv(*image.size)

        final_size = (target_size[0], round(target_size[0] / img_aspect))
        image = ImageOps.fit(image, final_size, method=Image.ANTIALIAS)

        # При сохранении PNG/GIF в JPEG прозрачный фон становится черным. Накладываем на фон
        if image.mode == 'RGBA' and target_format == 'JPEG':
            image = put_on_bg(image, final_size, target_bgcolor, masked=True)
    elif target_action == ACTION_INSCRIBE_RESIZE:
        image.thumbnail(target_size, resample=Image.ANTIALIAS)

        # При сохранении PNG/GIF в JPEG прозрачный фон становится черным. Накладываем на фон
        if image.mode == 'RGBA' and target_format == 'JPEG':
            image = put_on_bg(image, image.size, target_bgcolor, masked=True)
    elif target_action == ACTION_INSCRIBE:
        image.thumbnail(target_size, resample=Image.ANTIALIAS)

        # Наложение с маской для формата PNG вызывает потерю качества
        masked = image.mode == 'RGBA' and target_format != 'PNG'
        image = put_on_bg(image, target_size, target_bgcolor, masked=masked)
    
    return image
    
    
def variation_watermark(image, variation):
    """ Наложение водяного знака на картинку """
    wm_settings = variation.get('watermark', {})
    if not wm_settings:
        return image
    
    if image.mode != 'RGBA':
        image = image.convert('RGBA')
    
    with open(wm_settings['file'], 'rb') as fp:
        watermark = Image.open(fp)
        info = watermark.info
        if watermark.mode not in ('RGBA', 'LA') and not (watermark.mode == 'P' and 'transparency' in info):
            watermark.putalpha(255)
        
        img_width, img_height = image.size
        wm_width, wm_height = watermark.size
        
        scale = wm_settings['scale']
        if scale != 1:
            watermark = watermark.resize((int(wm_width*scale), int(wm_height*scale)), Image.ANTIALIAS)
            wm_width, wm_height = watermark.size
        
        position = wm_settings['position']
        padding = wm_settings['padding']
        if position == 'TL':
            left = padding[0]
            top = padding[1]
        elif position == 'TR':
            left = img_width - wm_width - padding[0]
            top = padding[1]
        elif position == 'BL':
            left = padding[0]
            top = img_height - wm_height - padding[1]
        elif position == 'BR':
            left = img_width - wm_width - padding[0]
            top = img_height - wm_height - padding[1]
        elif position == 'C':
            top = (img_height - wm_height) // 2
            left = (img_width - wm_width) // 2
        else:
            left = top = padding
            
        opacity = wm_settings['opacity']
        if opacity < 1:
            alpha = watermark.convert('RGBA').split()[3]
            alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
            watermark.putalpha(alpha)
        image.paste(watermark, (left, top), watermark)
    
    return image
    
    
def variation_overlay(image, variation):
    """ Наложение оверлея на картинку """
    overlay = variation['overlay']
    if not overlay:
        return image
    
    overlay_img = Image.open(overlay)
    if overlay_img.size != image.size:
        overlay_img = ImageOps.fit(overlay_img, image.size, method=Image.ANTIALIAS)

    # Дикие баги картинки, когда GIF => PNG + overlay
    if image.mode == 'P':
        image = image.convert('RGBA')

    image.paste(overlay_img, overlay_img)
    
    return image

    
def variation_mask(image, variation):
    """ Обрезка картинки по маске """
    target_bgcolor = variation.get('background')
    
    mask = variation['mask']
    if not mask:
        return image
        
    mask_img = Image.open(mask).convert('L')
    if mask_img.size != image.size:
        mask_img = ImageOps.fit(mask_img, image.size, method=Image.ANTIALIAS)

    background = Image.new("RGBA", image.size, target_bgcolor)
    image = Image.composite(image, background, mask_img)

    return image
